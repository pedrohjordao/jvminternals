---
theme: gaia
_class: lead
paginate: true
backgroundColor: #ffff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
footer: June 2020
marp: true
---

![left:30% 70%](assets/Duke.svg)

# **JVM Internals at a Glance**

###### JVM in Details: part 1 of N

Pedro Jordão

---

# Presenter

Pedro Jordão

* Software Developer for 6 years
* C++, Rust and Fortran (:weary:) for fast simulation code
* JVM based languages for backend and GUI
* Currently working with the Foresight team @criticaltechworks with Scala and Apache Flink and Spark

---

# This Series

* Part 0: Past, Present and Future
* Part 1: JVM Architecture <- Today!
* Part 2: JVM, JIT, AOT and YOU (Soon)
* Part 3: The Java Memory Model (or, How to Correctly Write a Singleton)
* Part 4: JIT Deep Dive? Garbage Collection? (Accepting Suggestion)

---

# What We Will Cover Today

* Compilers and Interpreters
* Java Bytecode
* JVM Architecture

---

# What We Will Not Cover Today

* Compilation into bytecode
* JIT and AOT

---

# Compilers vs Interpreters

* Roughly speaking, compilers transform code from a high level language into code of a lower level language 
  * The lower level language might be actual *plataform code*
    * e.g.: C compiles down to assembly and is then assembled into a binary
  * Compilation might take some time, but the execution is faster
* Interpreters execute code as they parse it 
  * No compilation time necessary
  * Slower runtime performance 

---

# Compilers vs Interpreters: Virtual Machines

* Languages such as Java and C# take the best of both worlds
  * Both compile down to *platform code* through an *Intermediate Language* 
  * Java's *platform*: *JVM*, *platform language*: *Java bytecode* (more commonly just *bytecode*)
  * C#'s *platform*: *Common Language Runtime*, *platform language*: *Common Intermediate Language* 
    * _not very creative if you ask me..._

---

# Compilers vs Interpreters: Virtual Machines

But we are talking about JVM, so we'll focus on that from now on!

---

# Compilers vs Interpreters: Virtual Machines

* Everything said before and from now on works the same for most JVM based language
  * Scala
  * Kotlin
  * Eta
* Clojure and Groovy are kinda of special, since they are interpreted

---

# Compilers vs Interpreters: Virtual Machines

* Ok, we have bytecodes, now what? 
* We need somewhere to execute this code 

---

# Enters the JVM

* The *Java Virtual Machine* is the one responsible for the "*run everywhere*" part in "*write once, run everywhere*"
* The JVM is responsible for _interpreting_ the bytecode and transforming into actual platform instructions
* Instead of needing compilers that are able to output low level instructions for different platforms you just need an implementation of the JVM for different platforms
* It might not seem like it, but this is actually easier to deal with

---

# Enters the JVM

* But aren't interpreters slow?
  * Yes
    * Sort of
      * Well, you see, it depends
          * Compered to what?
             * What about JIT?
               * Aren't we getting AOT now?
                 * Does project Valhalla helps with that?
                   * Who really killed JFK?
---

# Enters the JVM

* All good questions that we might answer at some point, but not necessarily today

---

# Lets Try Again

* Are interpreters slower than highly platform optimized binary code?
  * Yes
---

# Lets Try Again

* Is interpreting bytecode slow?
  * Not necessarily 
  * Remember, we are not having to interpret a whole high level language, but an intermediate language
  * As of Java 14 the JVM Specification defines less than 200 op codes for the JVM
  * Most deal with low level operations 
    * pop and push values from the stack


---

# Lets Try Again

* Yes, the JIT does help 
  * A lot, actually
* Yes, AOT is up and coming and might help even more (or not, more about that in the next talk)
* We will not cover this today

---

# The JVM
## Normal Workflow

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

![w:1000 center](assets/diagram-compilation.png)

---

# An Example
## Source Code

```
$ cat > HelloWorld.java << EOL
public class HelloWorld {
  private static final String TEXT = "Hello World";
  public static void main(String[] args) {
    System.out.println(TEXT);
  }
}
EOL
$ javac HelloWorld.java
```
---
# An Example
## Decompiled Bytecode

```
$ less HelloWorld.class
```

Well, that doesn't help at all...

```
$ xdd HelloWorld.class
```

A little better

---
# An Example
## Decompiled Bytecode

```
$ javap -c -v -l HelloWorld.class | less
```

Much better

---

# Now For an Bird's Eye View of The JVM

## JVM Components

* Class Loader (We will mostly focus here today)
* Runtime Data
* Execution Engine

---

# JVM Architecture

![h:450 center](assets/jvm.png)

---

# JVM Architecture
## Class Loader

* Responsible for... loading classes in three steps:
  * Loading
  * Linking 
  * Initializing

---
# JVM Architecture
## Class Loader

* Loading
  * Loads the`.class` file (bytecode), usually beginning with the one containing `main(..)`
    * Recursive loading - loads full hierarchy (if necessary) and fields (if necessary)
  * Classes are only loaded when they are referred to at the first time *at runtime*

---
# JVM Architecture
## Class Loader

* There are two type of class loaders:
  * Bootstrap (Supplied by the JVM)
  * User defined
    * Subclasses of `ClassLoader`
    * May read from the filesystem, network or generate classes dynamically 

---
# JVM Architecture
## Class Loader

* Class loaders work as a hierarchy 
* The most common class loaders are
  * Bootstrap: Loads the contents of `lib/rt.jar`
  * Extension: Loads the contents of `lib/ext`
  * System: Loads the contents of the classpath
  * OSGi: standard/framework for modular Java packages

---
# JVM Architecture
## Class Loader

* Class loading principles:
  * Delegation
    * Each class loader will ask for the parent class to load a given class. Only if the parent class loader is not able to load the class the child class loader makes an attempt

---
# JVM Architecture
## Class Loader

* Class loading principles:
  * Visibility
    * Child class loaders can see parent's loaded classes, but not the opposite

---
# JVM Architecture
## Class Loader

* Class loading principles:
  * Uniqueness
    * A class may only be loaded once _in a class loader hierarchy_
    * The last point is very often misunderstood as only once in the JVM
---
# JVM Architecture
## Class Loader

* Class loading principles:
  * Uniqueness
    * The fact that the same class might be loaded multiple times in the same JVM is a major blow for AOT compilation
      * If you want to know more about that come to my next talk soon-ish!
    * At runtime the _identity_ of a class is defined by it's FQN **and** it's class loader
---
# JVM Architecture
## Class Loader

* If you ever worked with J2EE frameworks, different applications within the same JVM are loaded by different class loaders so no conflicts happen
* Imagine application `A` needs to use library version `1.0` while application `B` uses version `2.0`. Multiple class loaders solve this issue
* If you haven't worked with J2EE, _I envy you_

---
# JVM Architecture
## Class Loader

* Linking 
  * Verifies correctness of the bytecode
  * Allocates necessary memory for internal representation
  * Resolution of references

---
# JVM Architecture
## Class Loader

* The main job of the linking step is making sure the the binary representation of the class is valid
* It also makes sure that class loaders in the same hierarchy only have one instance of each class definition. The mechanics about how this is done are out of the scope of this presentation (i.e. it is really confusing and I'm not sure I understand it)

---
# JVM Architecture
## Class Loader

* Initialization 
  * Initializes static data
  * Runs constructors
  * Process goes from parent to child, in order of declaration
---
# JVM Architecture
## Class Loader

* Initialization of the class (not the object!) occurs the first time the class is referenced through a `new`, static method or some reflection.
* Because the JVM is multithreaded, there are synchronization locks in place to make sure initialization of a class (not object!) takes place only once per class loader.
---
# JVM Architecture
## Class Loader

* Let's talk about Singletons
---
# JVM Architecture
## Class Loader

* Let's talk about Singletons
  * Don't use them!
---
# JVM Architecture
## Class Loader

* But if you really must...

```java
class Singleton {
  private static INSTANCE = null;
  private Singleton() {}
  public static Singleton getInstance() {
    if (instance == null) INSTANCE = new Singleton();
    return INSTANCE;
  }
}
```
---
# JVM Architecture
## Class Loader

* Don't do this!
  * `getInstance` has no synchronization, `Singleton`'s constructor might be called more than once in different threads, returning unexpected results (or even uninitialized objects) 
  * You can fix this with `synchronized`

---
# JVM Architecture
## Class Loader

```java
class Singleton {
  private static INSTANCE = null;
  private Singleton() {}
  public static synchronized Singleton getInstance() {
    if (instance == null) INSTANCE = new Singleton();
    return INSTANCE;
  }
}
```
---
# JVM Architecture
## Class Loader

* Don't do that either!
  * While "correct", you are paying the price of locking the whole class every time you access it, for something that should require locking only once (the initialization)
  * No, not even _Double Check Locking_ is guaranteed to work, but the reasons for that are more complicated 

---
# JVM Architecture
## Class Loader

* Correct implementation

```java
class Singleton {
  private static final INSTANCE = new Singleton();
  private Singleton() {}
  public static Singleton getInstance() {
    return INSTANCE;
  }
}
```
---
# JVM Architecture
## Class Loader

* Cleaner, no explicit locking, guaranteed to work
* `static final` fields are initialized while the class loader holds a lock for the initialization of the class (same for`static` blocks)
* The _"singleton enum"_ pattern works the same way for the same reasons
* If you want to know more about that, come to may next-next talk that will happen... eventually...

---
# JVM Architecture
## Now In Our Example

* When running with `java HelloWorld` the JVM reads the `.class` file 

![h:300 center](assets/raw-classfile.png)

---
# JVM Architecture
## Now In Our Example

*  Linker validates bytecode

![center](assets/linker-info.png)

---
# JVM Architecture
## Now In Our Example

* Are the Major and Minor Version Valid?

![center](assets/major-minor.png)

---
# JVM Architecture
## Now In Our Example

* Does it start with the magic number?

![center](assets/cafebabe.png)

---
# JVM Architecture
## Now In Our Example

* Constant pool elements point to correct values?

![center](assets/parameters.png)

---
# JVM Architecture
## Now In Our Example

* What other classes we need to load?

![center](assets/class-load.png)

---
# JVM Architecture
## Now In Our Example

* Finally, initialization takes place and static data is initialized

![center](assets/constructor.png)


---
# JVM Architecture
## Runtime Data

* The runtime data area contains 
  * Method Area - One Per JVM (i.e. Linking must be thread safe)
    * Runtime Constant Pool
    * Field Data
    * Method Data
    * Method Code 
---
# JVM Architecture
## Runtime Data

* The runtime data area contains 
  * Stack Area - One Per Thread
    * Each thread creates a new Stack Area
    * Each method call pushes a new Stack Frame into the Stack Area
---
# JVM Architecture
## Runtime Data

* The runtime data area contains 
  * PC Register - One Per Thread
    * Tracks the address of the current instruction per thread

---
# JVM Architecture
## Runtime Data

* The runtime data area contains 
  * Native Method Stack - One Per Thread
    * When a Java method invokes a native method, it leaves its thread's Stack Area and moves into the Native Method Stack, where anything goes 

---
# JVM Architecture
## Back to the example 

* Method Code

![center](assets/main.png)

---

# JVM Architecture
## Execution Engine

* This is where all the fun happens and code executes
  * Interpreter 
  * JIT
  * Garbage Collection

---

# JVM Architecture
## Execution Engine

* Interpreter
  * Executes instructions one by one from the Method Area
  * It is really *really* just an interpreter

---

# JVM Architecture
## Execution Engine

* JIT - Just In Time Compiler
  * We'll go more in depth next time
  * In short: conditionally compiles (and decompiles!) hot paths in the code to native code

---

# JVM Architecture
## Execution Engine

* Garbage Collector 
  * Cleans objects that do not have any more references to it
  * Automatic memory management 
  * Many different implementations and algorithms
  * Probably the most important piece to optimize 


---
# Q&A
